const {app} = require('electron')
const cheerio = require('cheerio')
const Proxy = require('http-mitm-proxy')
const logger = require('electron-log')
const moment = require('moment')
const fs = require('fs-extra')
const vm = require('vm')
const rp = require('request-promise')

class SeikenProxy {
  constructor (mainWindow, gameWindow) {
    this.handlers = Object()
    this.apiHandlers = Object()
    this.proxy = Proxy()
    this.mainWindow = mainWindow
    this.gameWindow = gameWindow
    let sandbox = {CryptoJS: require('crypto-js')}
    this.sandbox = sandbox
    vm.createContext(sandbox)

    const url = 'http://seimani-prod-lb-01-1099898124.ap-northeast-1.elb.amazonaws.com/seimani/js/CryptoHelper.js'
    rp.get(url).then(function (js) {
      vm.runInContext(js, sandbox)
    })
  }

  addHandler (urlPrefix, callback) {
    this.handlers[urlPrefix] = callback
  }

  addApiHandler (api, callback) {
    this.apiHandlers[api] = callback
  }

  addGadgetHandler (callback) {
    this.gadgetHandler = callback
  }

  withParsedResponseBody (ctx, cb) {
    var chunks = []
    ctx.onResponseData((ctx, chunk, callback) => {
      chunks.push(chunk)
      return callback(null, chunk)
    })
    ctx.onResponseEnd((ctx, callback) => {
      let body = Buffer.concat(chunks).toString()
      cb(body)
      return callback()
    })
  }

  withParsedRequestBody (ctx, cb) {
    var chunks = []
    ctx.onRequestData((ctx, chunk, callback) => {
      chunks.push(chunk)
      return callback(null, chunk)
    })
    ctx.onRequestEnd((ctx, callback) => {
      let body = Buffer.concat(chunks).toString()
      cb(body)
      return callback()
    })
  }

  start (port) {
    if (!port) {
      port = 8081
    }
    this.addHandler('/gadgets/ifr', (ctx) => {
      logger.log('gadget call')
      var chunks = []
      ctx.onResponseData((ctx, chunk, callback) => {
        chunks.push(chunk)
        return callback(null, null)
      })
      ctx.onResponseEnd((ctx, callback) => {
        let body = Buffer.concat(chunks).toString()
        var $ = cheerio.load(body)
        $('#under_game').remove()
        $('head').append('<style>::-webkit-scrollbar{display:none}</style>')
        ctx.proxyToClientResponse.write($.html())
        if (this.gadgetHandler) this.gadgetHandler().then(() => true)
        return callback()
      })
    })

    var apiHandlers = this.apiHandlers
    this.addHandler('/seimani/api/v1', (ctx) => {
      var api = ctx.clientToProxyRequest.url.replace('/seimani/api/v1', '')
      logger.log('API call', api)

      if (!this.sandbox.CryptoHelper) {
        return
      }

      var req
      this.withParsedRequestBody(ctx, (body) => {
        var encrypted = body
        if (encrypted.length > 0) {
          req = this.sandbox.CryptoHelper.aesDecrypt(encrypted)
        } else {
          req = Object()
        }
        if (apiHandlers[api]) {
          logger.log('API matched', api)
        } else {
          logger.log('API REQ: ', api, req)
        }
      })
      this.withParsedResponseBody(ctx, (body) => {
        var encrypted = body
        var decrypted = this.sandbox.CryptoHelper.aesDecrypt(encrypted)
        var res = decrypted
        if (apiHandlers[api]) {
          logger.log(`API[${api}] called`)
          try {
            apiHandlers[api](req, res)
          } catch (e) {
            logger.error('API callback error', e)
          }
        }
        if (decrypted.error === 0) {
          this.mainWindow.webContents.send(`api:complete:${api}`, req, res)
          this.gameWindow.webContents.send(`api:complete:${api}`, req, res)

          var apiLogDir = `${app.getPath('userData')}/log/api${api}`
          var baseFileName = `${moment().format('YYYYMMDD-HHmmss-SSS')}`
          var reqFileName = `${apiLogDir}/${baseFileName}-req.json`
          var resFileName = `${apiLogDir}/${baseFileName}-res.json`
          logger.log(`API RES, ${api} save to log/api/`)
          fs.mkdirsSync(apiLogDir)
          fs.writeFile(reqFileName, JSON.stringify(req), (err) => { if (err) { logger.error(err) } })
          fs.writeFile(resFileName, JSON.stringify(res), (err) => { if (err) { logger.error(err) } })
        }
      })
    })

    this.port = port
    this.proxy.use(Proxy.gunzip)
    this.proxy.use(Proxy.wildcard)

    var handlers = this.handlers

    this.proxy.onError(function (ctx, err, errorKind) {
      // ctx may be null
      var url = (ctx && ctx.clientToProxyRequest) ? ctx.clientToProxyRequest.url : ''
      logger.error(errorKind + ' on ' + url + ':', err)
    })

    this.proxy.onRequest(function (ctx, callback) {
      var url = ctx.clientToProxyRequest.url
      logger.log('onRequest', url)
      Object.keys(handlers).forEach((urlPrefix) => {
        if (url.startsWith(urlPrefix)) {
          logger.log('handler matched', urlPrefix)
          handlers[urlPrefix](ctx)
        }
      })
      return callback()
    })

    this.proxy.onResponse(function (ctx, callback) {
      console.log('onResponse', ctx.clientToProxyRequest.url)
      if (ctx.proxyToClientResponse.statusCode !== 200) {
        logger.log('onResponse', ctx.proxyToClientResponse.url, ctx.proxyToClientResponse.statusCode)
      }
      return callback()
    })

    this.proxy.listen({port: this.port, keepAlive: true})
    logger.log('proxy listening on ' + this.port)
  }

  stop () {
    this.proxy.close()
  }
}

export default SeikenProxy
