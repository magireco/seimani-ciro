'use strict'

import { app, BrowserWindow } from 'electron'
import logger from 'electron-log'
import windowStateKeeper from 'electron-window-state'
import Proxy from './seikenproxy'

const proxyPort = 8084

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
let gameWindow

const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080/`
  : `file://${__dirname}/index.html`

const gameWinURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9081/game.html`
  : `file://${__dirname}/game.html`

function createWindow () {
  logger.log('starting...')
  let mainWindowState = windowStateKeeper({
    file: 'main-window-state.json',
    defaultWidth: 800,
    defaultHeight: 600
  })

  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    width: mainWindowState.width,
    height: mainWindowState.height,
    x: mainWindowState.x,
    y: mainWindowState.y,
    webPreferences: {
      allowRunningInsecureContent: true,
      warnAboutInsecureContentAllowed: false,
      webSecurity: false
    }
  })
  mainWindowState.manage(mainWindow)

  let gameWindowState = windowStateKeeper({
    file: 'game-window-state.json'
  })

  gameWindow = new BrowserWindow({
    width: 1136,
    height: 640,
    x: gameWindowState.x,
    y: gameWindowState.y,
    useContentSize: true,
    resizable: false,
    autoHideMenuBar: true,
    closable: false,
    frame: false,
    webPreferences: {
      allowRunningInsecureContent: true,
      warnAboutInsecureContentAllowed: false,
      webSecurity: false
    }
  })
  gameWindowState.manage(gameWindow)

  let proxy = new Proxy(mainWindow, gameWindow)
  proxy.start(proxyPort)

  mainWindow.loadURL(winURL)

  gameWindow.webContents.session.setProxy({proxyRules: `localhost:${proxyPort}`}, () => { return true })

  gameWindow.loadURL(gameWinURL)

  mainWindow.on('closed', () => {
    mainWindow = null
    if (gameWindow) {
      gameWindow.close()
      gameWindow.destroy()
    }
  })

  gameWindow.on('closed', () => {
    gameWindow = null
    if (mainWindow) {
      mainWindow.close()
      mainWindow.destroy()
    }
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
