const state = {
  NowInBattle: false,
  StageId: ''
}

const mutations = {
  setNowInBattle (state, value) {
    state.NowInBattle = value
  },
  setStageId (state, value) {
    state.StageId = value
  }
}

const actions = {
  startBattle ({ commit }, req, res) {
    commit('setNowInBattle', true)
    commit('setStageId', req.challenge_stage_id)
  },
  finishBattle ({ commit }, req, res) {
    commit('setNowInBattle', false)
    commit('setStageId', '')
  }
}

const getters = {
  NowInBattle (state) {
    return state.NowInBattle
  },
  StageId (state) {
    return state.StageId
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
