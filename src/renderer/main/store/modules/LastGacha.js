const state = {
  LastGachaSku: null,
  LastGachaResult: null
}

const mutations = {
  setLastGachaSku (state, skuId) {
    state.LastGachaSku = skuId
  },
  setLastGachaResult (state, result) {
    state.LastGachaResult = result
  }
}

const actions = {
  setLastGachaSku ({ commit }, skuId) {
    commit('setLastGachaSku', skuId)
  },
  setLastGachaResult ({ commit }, result) {
    commit('setLastGachaResult', result)
  }
}

const getters = {
  LastGachaSku (state) {
    return state.LastGachaSku
  },
  LastGachaResult (state) {
    return state.LastGachaResult
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
