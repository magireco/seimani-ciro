const state = {
  muteState: false
}

const mutations = {
  toggleMute (state) {
    state.muteState = !state.muteState
  }
}

const actions = {
  toggleMute (context) {
    context.commit('toggleMute')
  }
}

const getters = {
  getMuteStatus (state) {
    return state.muteState
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
