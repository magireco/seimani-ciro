const state = {
  AffairState: null
}

const mutations = {
  setAffairState (state, result) {
    state.AffairState = result
  }
}

const actions = {
  setAffairState ({ commit }, result) {
    commit('setAffairState', result)
  }
}

const getters = {
  AffairState (state) {
    return state.AffairState
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
