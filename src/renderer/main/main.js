import Vue from 'vue'
import Buefy from 'buefy'
import AsyncComputed from 'vue-async-computed'
import rp from 'request-promise'

import App from './App'
import router from './router/index'
import store from './store/index'
import 'font-awesome/scss/font-awesome.scss'
import logger from 'electron-log'

Vue.use(AsyncComputed)
Vue.use(Buefy)

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = rp
Vue.config.productionTip = false
Vue.logger = Vue.prototype.$logger = logger

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
